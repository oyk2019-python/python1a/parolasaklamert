# parolalar isimli boş bir dizi oluşturuldu.
parolalar = []
#True while döngüsü açıldı, false olana kadar çalışır.
while True:
    ## Kullanıcıdan bir işlem yapmasını ister.
    print('Bir işlem seçin')
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :')
    #burada if döngüsüne girer, ilk kontrol olarak kullanıcının bir string mi yoksa integer değer mi girdi
    # o kontrol edilir, eğer işlem integer ise bi alt if döngüsüne devam eder.
    if islem.isdigit():
        islem_int = int(islem)
        # islem_int'de kontrol olarak, kullanıcıya sorulan 1 2'den başka bir değer girip girmediğini kontrol eder.
        # kullanıcı eğer 1 2den farklı bir sayı girerse print ile hata verilir. eğer 1 veya 2 girdiyse continue ile
        # döngü devam eder.
        if islem_int not in [1, 2]:
            print('Hatalı işlem girişi')
            continue
        # eğer kullanıcı işlemlerden ikincisini seçtiyse, bu if kontrolü içine girer.
        if islem_int == 2:
            #kullanıcıdan gerekli verilerin girilmesini ister.
            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')

            kullanici_adi = input('Kullanici Adi Girin :')
            parola = input('Parola :')
            parola2 = input('Parola Yeniden :')
            eposta = input('Kayitli E-posta :')
            gizlisorucevabi = input('Gizli Soru Cevabı :')

            #her bir kontrolde eğer kullanıcı boş bir değer girdiyse kontrol eder ve hata verir.
            #eğer her bir girdi için boşluk yoksa döngü devam eder.

            if kullanici_adi.strip() == '':
                print('kullanici_adi girmediz')
                continue
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:
                print('Parolalar eşit değil')
                continue
            #burada yeni_girdi adında key, value olarak bir liste tutulur. Şimdilik bu liste boştur.
            # her bir key e karşılık inputtan gelen value değerler eklenir.
            yeni_girdi = {
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            #inputtan gelen girdiler bu listeye eklenir.
            parolalar.append(yeni_girdi)
            continue
            # eğer kullanıcı birinci işlemi yapmışsa.
        elif islem_int == 1:
            #parola_no 0'dan başlatır. çünkü kayıt edilirken indeks 0'dan başlar.
            alt_islem_parola_no = 0
            #for parola seçilen parolalar listesindeyse for dönügüsü çalışır.
            for parola in parolalar:
                alt_islem_parola_no += 1
                #burada print ile key value olan değerler get ile çekilir ve kullanıcıya gösterilir.
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))

            alt_islem = input('Yukarıdakilerden hangisi ?: ')
            # string mi integer değer mi girildi bunun kontrolü.
            if alt_islem.isdigit():
                #burada kullanıcı eğer listede var olan paraola sayısında daha az veya daha fazla değer girip girmediğini
                #kontrol edeer. liste sayısından farklı iste hata verir. tekrardan sorar.
                if int(alt_islem) < 1 or len(parolalar) - 1 < int(alt_islem):
                    print('Hatalı parola seçimi')
                    continue
                # -1 ile işlemler 0'dan başladığı için aynı duruma getirilir.
                parola = parolalar[int(alt_islem) - 1]
                #kullanıcıya gösterilir.
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue

    print('Hatalı giriş yaptınız')
